﻿import { PipeTransform, Pipe } from '@angular/core';
import { IRole } from '../models/role';

@Pipe({
    name: 'roleFilter'
})
export class RoleFilterPipe implements PipeTransform {

    transform(value: IRole[], filterBy: string): IRole[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? value.filter((role: IRole) =>
            role.role_name.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}
