﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { RoleListComponent } from './role-list.component';
import { RoleDetailComponent } from './role-detail.component';
import { RoleEditComponent } from './role-edit.component';
import { RoleFilterPipe } from './role-filter.pipe';

import { RoleDetailGuard, RoleEditGuard } from './role-guard.service';
import { RoleService } from './role.service';
import { SharedModule } from '../shared/shared.module';

import { AuthGuard } from '../auth/auth.guard';

@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'roles', component: RoleListComponent, canActivate: [AuthGuard] },
            { path: 'role/:id', component: RoleDetailComponent, canActivate: [RoleDetailGuard, AuthGuard] },
            { path: 'roleEdit/:id', component: RoleEditComponent, canDeactivate: [RoleEditGuard, AuthGuard] }
        ])
    ],
    entryComponents: [

    ],
    declarations: [
        RoleListComponent,
        RoleDetailComponent,
        RoleEditComponent,
        RoleFilterPipe
    ],
    providers: [
        RoleService,
        RoleDetailGuard,
        RoleEditGuard,
        AuthGuard,
    ]
})
export class RoleModule { }
