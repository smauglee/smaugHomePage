﻿import { Component, OnInit } from '@angular/core';

import { IRole } from '../models/role';
import { RoleService } from './role.service';

@Component({
    templateUrl: './role-list.component.html',
    styleUrls: ['./role-list.component.css']
})
export class RoleListComponent implements OnInit {
    pageTitle: string = 'Role List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string;
    errorMessage: string;

    roles: IRole[];

    constructor(private _roleService: RoleService,

    ) {

    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void {

        this._roleService.getRoles()
            .subscribe(roles => this.roles = roles,
            error => this.errorMessage = <any>error);

    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Role List: ' + message;
    }
}
