﻿import { Component, OnInit, OnDestroy, AfterViewInit, ViewChildren, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { ISelectList } from '../shared/selectList';
import { IUser } from '../models/user';
import { UserService } from './user.service';

import { NumberValidator } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';


@Component({
    templateUrl: './user-edit.component.html'
})
export class UserEditComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];

    pageTitle: string = 'User Detail';
    user: IUser;
    divisionSelectList: ISelectList[];
    roleSelectList: ISelectList[];
    errorMessage: string;
    userForm: FormGroup;

    private sub: Subscription;



    // Use with the generic validation message class
    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    constructor(private fb: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private _userService: UserService) {

        // Defines all of the validation messages for the form.
        // These could instead be retrieved from a file or database.
        this.validationMessages = {
            user_id: {
                required: 'user id is required.'
            },
            user_name: {
                required: 'user name is required.',
                minlength: 'user name must be at least three characters.',
                maxlength: 'user name cannot exceed 50 characters.'
            },
            email: {
                required: 'email is required.'
            },
            role_id: {
                required: 'role is required.'
            },
            division_id: {
                required: 'division is required.'
            }
        };

        // Define an instance of the validator for use with this form, 
        // passing in this form's set of validation messages.
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit(): void {
        this.userForm = this.fb.group({
            user_id: [{ value: '' }, Validators.required],
            user_name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
            email: ['', Validators.required],
            role_id: ['', Validators.required],
            tel_no: ['', Validators.required],
            division_id: ['', Validators.required],
            birthday: ['', Validators.required],
            sort_no: [{ value: '' }],
            password: [{ value: '' }],
            create_user_id: [{ value: '' }],
            create_date: [{ value: '' }],
            update_user_id: [{ value: '' }],
            update_date: [{ value: '' }]
            //starRating: ['', NumberValidators.range(1, 5)],
            //tags: this.fb.array([]),
            //description: ''
        });

        this.sub = this.route.params.subscribe(
            params => {
                let id = params['id'];
                this.getDivisionSelectList();
                this.getRoleSelectList();
                this.getUser(id);
            });
    }



    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.userForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.userForm);
        });
    }


    getUser(id: string) {
        this._userService.getUser(id).subscribe(
            //user => this.user = user,
            (user: IUser) => this.onUsertRetrieved(user),
            error => this.errorMessage = <any>error);
    }

    getDivisionSelectList() {
        this._userService.getDivisionList().subscribe(
            selectList => this.divisionSelectList = selectList,
            error => this.errorMessage = <any>error);
    }

    getRoleSelectList() {
        this._userService.getRoleList().subscribe(
            selectList => this.roleSelectList = selectList,
            error => this.errorMessage = <any>error);
    }

    onUsertRetrieved(user: IUser): void {
        if (this.userForm) {
            this.userForm.reset();
        }
        this.user = user;

        if (this.user.user_id === null) {
            this.pageTitle = 'Add User';
        } else {
            this.pageTitle = `Edit User: ${this.user.user_name}`;
            //this.userForm.controls["user_id"].setValue({ value: '', disabled: true }, { onlySelf: true });
            let ctrl = this.userForm.get('user_id').disable();
            //ctrl.enabled ? ctrl.disable() : ctrl.enable()
        }

        // Update the data on the form
        this.userForm.patchValue({
            user_id: this.user.user_id,
            user_name: this.user.user_name,
            email: this.user.email,
            division_id: this.user.division_id,
            role_id: this.user.role_id,
            tel_no: this.user.tel_no,
            //birthday: this.user.birthday ,
            birthday: this.user.birthday,
            sort_no: this.user.sort_no,
            password: this.user.password,
            create_user_id: this.user.create_user_id,
            create_date: this.user.create_date,
            //description: this.user.description
        });

    }

    deleteUser(): void {
        if (this.user.user_id == "") {
            // Don't delete, it was never saved.
            this.onSaveComplete();
        } else {
            if (confirm(`Really delete the user: ${this.user.user_name}?`)) {
                this._userService.deleteUser(this.user.user_id)
                    .subscribe(
                    () => this.onSaveComplete(),
                    (error: any) => this.errorMessage = <any>error
                    );
            }
        }
    }

    saveUser(): void {
        if (this.userForm.dirty && this.userForm.valid) {
            // Copy the form values over the user object values
            let p = Object.assign({}, this.user, this.userForm.value);

            this._userService.saveUser(p)
                .subscribe(
                () => this.onSaveComplete(),
                (error: any) => this.errorMessage = <any>error
                );
        } else if (!this.userForm.dirty) {
            this.onSaveComplete();
        }
    }

    onSaveComplete(): void {
        // Reset the form to clear the flags
        this.userForm.reset();
        this.router.navigate(['/users']);
    }

    onBack(): void {
        this.router.navigate(['/users']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'User Detail: ' + message;
    }
}
