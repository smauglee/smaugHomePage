﻿import {
    Component, OnInit, AfterViewInit, OnDestroy, ViewChildren,
    ElementRef, ViewContainerRef, trigger, state, style, transition, animate, keyframes
} from '@angular/core';
import { FormBuilder, FormGroup, FormArray, Validators, FormControlName } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { UserService } from './user.service';
import { IUser } from '../models/user';


@Component({
    templateUrl: 'app/user/user.component.html'
})
export class UserComponent implements OnInit {
    public pageTitle: string = 'Welcome to Smaug';
    userForm: FormGroup;
    

    constructor(private fb: FormBuilder) { }
    ngOnInit(): void {
        this.userForm = this.fb.group({
            user_id: '',
            user_name: '',
            email: '',
            password: '',
            role_id: '',
            role_name: '',
            division_id: '',
            division_name: '',
            tel_no: '',
            birthday: '',
            post_no: '',
            city_id: '',
            city_name: '',
            address_1: '',
            address_2: '',
            sort_no: '',
            bank_id: '',
            bank_name: '',
            bank_account_type: '',
            bank_account_no: '',
            bnak_branch_no: '',
            bank_branch_name: ''
        });
    }
}
