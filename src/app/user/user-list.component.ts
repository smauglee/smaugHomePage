﻿import { Component, OnInit }  from '@angular/core';

import { IUser } from '../models/user';
import { UserService } from './user.service';

@Component({
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
    pageTitle: string = 'User List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string;
    errorMessage: string;

    users: IUser[];

    constructor(private _userService: UserService,
        
    ) {

    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void {
       
        this._userService.getUsers()
                .subscribe(users => this.users = users,
            error => this.errorMessage = <any>error);

    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'User List: ' + message;
    }
}
