﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { UserListComponent } from './user-list.component';
import { UserDetailComponent } from './user-detail.component';
import { UserEditComponent } from './user-edit.component';
import { UserFilterPipe } from './user-filter.pipe';

import { UserDetailGuard, UserEditGuard } from './user-guard.service';
import { UserService } from './user.service';
import { SharedModule } from '../shared/shared.module';

import { AuthGuard } from '../auth/auth.guard';

@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'users', component: UserListComponent, canActivate: [AuthGuard] },
            { path: 'user/:id', component: UserDetailComponent, canActivate: [UserDetailGuard, AuthGuard] },
            { path: 'userEdit/:id', component: UserEditComponent, canDeactivate: [UserEditGuard, AuthGuard] }
        ])
    ],
    entryComponents: [

    ],
    declarations: [
        UserListComponent,
        UserDetailComponent,
        UserEditComponent,
        UserFilterPipe
    ],
    providers: [
        UserService,
        UserDetailGuard,
        UserEditGuard,
        AuthGuard,
    ]
})
export class UserModule { }
