﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs/Subscription';

import { IUser } from '../models/user';
import { UserService } from './user.service';

@Component({
    templateUrl: './user-detail.component.html'
})
export class UserDetailComponent implements OnInit, OnDestroy {
    pageTitle: string = 'User Detail';
    user: IUser;
    errorMessage: string;
    private sub: Subscription;

    constructor(private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService) {
    }

    ngOnInit(): void {
        this.sub = this._route.params.subscribe(
            params => {
                let id = params['id'];
                this.getUser(id);
            });
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }

    getUser(id: string) {
        this._userService.getUser(id).subscribe(
            user => this.user = user,
            error => this.errorMessage = <any>error);
    }

    onBack(): void {
        this._router.navigate(['/users']);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'User Detail: ' + message;
    }
}
