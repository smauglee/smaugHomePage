﻿import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, CanDeactivate } from '@angular/router';

import { UserEditComponent } from './user-edit.component';

@Injectable()
export  class UserDetailGuard implements CanActivate {

    constructor(private _router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot): boolean {
        let id = route.url[1].path;
        if (id === "") {
            alert('Invalid user Id');
            // start a new navigation to redirect to list page
            this._router.navigate(['/users']);
            // abort current navigation
            return false;
        };
        return true;
    }
}

@Injectable()
export class UserEditGuard implements CanDeactivate<UserEditComponent> {

    canDeactivate(component: UserEditComponent): boolean {
        if (component.userForm.dirty) {
            let user_name = component.userForm.get('user_name').value || 'New User';
            return confirm(`Navigate away and lose all changes to ${user_name}?`);
        }
        return true;
    }
}
