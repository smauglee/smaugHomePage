﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './home/welcome.component';

import { SharedModule } from './shared/shared.module';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { DivisionModule } from './division/division.module';
import { RoleModule } from './role/role.module';
@NgModule({
  declarations: [
      AppComponent,
      WelcomeComponent,
  ],
  imports: [
      BrowserModule,
      RouterModule.forRoot([
          { path: 'welcome', component: WelcomeComponent },
          { path: '', redirectTo: 'welcome', pathMatch: 'full' },
          { path: '**', redirectTo: 'welcome', pathMatch: 'full' },
      ], { useHash: true }),
    FormsModule,
    HttpModule,
      BrowserAnimationsModule,
      SharedModule,
      AuthModule,
      UserModule,
      DivisionModule,
      RoleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
