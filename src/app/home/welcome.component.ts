﻿import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes,group } from '@angular/animations';

@Component({
    templateUrl: 'welcome.component.html',
    animations: [
        trigger('imgTrigger', [
            //state('init', style({
            //    transform: 'scale(1)'
            //})),
            //state('dest', style({
            //    transform: 'scale(0.9)'
            //})),
            //state('big-dest', style({
            //    transform: 'scale(1)'
            //})),
            //transition('init => dest,dest => init', animate('500ms ease-in')),
            //transition('* => *', [
            //    animate(500, keyframes([
            //        style({ opacity: 1, transform: 'scale(1)' }),
            //        style({ opacity: 1, transform: 'scale(0.9)' }),
            //        style({ opacity: 1, transform: 'scale(1)' }),
            //        style({ opacity: 1, transform: 'scale(0.9)' }),
            //        style({ opacity: 1, transform: 'scale(1)' })
            //    ]))
            //])
            transition('* => *', [
                style({ transform: 'scale(.9)', opacity: 0 }),
                group([
                    animate('0.1s ease', style({
                        transform: 'scale(0.9)'
                    })),
                    animate('0.5s ease', style({
                        transform: 'scale(1)'
                    })),
                    animate('0.1s ease', style({
                        transform: 'scale(0.9)'
                    })),
                    animate('0.5s ease', style({
                        transform: 'scale(1)'
                    }))
                ])
            ]),
        ])
    ]
})
export class WelcomeComponent implements OnInit {
    public pageTitle: string = 'Welcome to Smaug';

    constructor() { }

    ngOnInit(): void {

    }
    state: string = 'init';
    toggleState(): void {
        this.state = (this.state === "dest" ? "init" : "dest");
    }
    
}
