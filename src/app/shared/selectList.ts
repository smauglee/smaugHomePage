﻿/* Defines the select list entity */
export interface ISelectList {
    value: string;
    display: string;
}

