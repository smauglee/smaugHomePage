﻿export class Const {
    //public static BASE_ENDPOINT = 'http://samugserver.azurewebsites.net/api/';
    public static BASE_ENDPOINT = 'http://localhost:50158/api/';
    public static CONTENT_TYPE = 'Content-Type';
    public static JSON_TYPE = 'application/json';
    public static URL_ENCODE_TYPE = 'application/x-www-form-urlencoded';
}