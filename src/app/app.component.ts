﻿import { Component, OnInit, Input } from '@angular/core';
import { trigger, state, style, transition, animate, keyframes,group } from '@angular/animations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
      trigger('menuTrigger', [
          state('init', style({
              transform: 'translateX(0px)'
            })),
          state('dest', style({
              transform: 'translateX(-200px)'
            })),
            transition('init => dest,dest => init', animate('500ms ease-in')),
      ]),
  ]
})
export class AppComponent {
    state: string = 'init';
    active: boolean = true;
    toggleMenu(): void {
        this.state = (this.state === "dest" ? "init" : "dest");
    }
}
