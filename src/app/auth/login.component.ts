﻿import {
    Component, OnInit, AfterViewInit, OnDestroy, ViewChildren,
    ElementRef, ViewContainerRef, trigger, state, style, transition, animate, keyframes
} from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormArray, Validators, FormControlName } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/merge';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { NumberValidator } from '../shared/number.validator';
import { GenericValidator } from '../shared/generic-validator';

import { ILogin } from '../models/login';

import { AuthService } from './auth.service';


@Component({
    //moduleId: module.id,
    templateUrl: './login.component.html',
})

export class LoginComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChildren(FormControlName, { read: ElementRef }) formInputElements: ElementRef[];
    loginForm: FormGroup;
    private sub: Subscription;
    //model: any = {};
    loading = false;
    error = '';
    errorMessage: string;
    login: ILogin;
    sendButtonText: string = "Login";

    displayMessage: { [key: string]: string } = {};
    private validationMessages: { [key: string]: { [key: string]: string } };
    private genericValidator: GenericValidator;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private _elementRef: ElementRef,
        private authService: AuthService,
        private fb: FormBuilder) {
        this.validationMessages = {
            username: {
                required: 'username is required.',
                maxlength: 'username cannot exceed 20 characters.'
            },
            password: {
                required: 'password is required.'
            }
        };
        this.genericValidator = new GenericValidator(this.validationMessages);
    }

    ngOnInit(): void {
        // reset login status
        //this.authenticationService.logout();
        //this.slimLoad.complete();
        //this.loginForm = this.fb.group({
        //    user_id: '',
        //    user_name: '',
        //    password: ''
        //});
        this.loginForm = this.fb.group({
            username: ['', [Validators.required, Validators.maxLength(20)]],
            password: ['', Validators.required],
            //client_id: ['client_id'],
            grant_type: ['password'],
            //client_secret: ['client_secret'],
            //ClientId: ['test']
        });
    }

    ngOnDestroy(): void {
        //this.sub.unsubscribe();
    }

    ngAfterViewInit(): void {
        // Watch for the blur event from any input element on the form.
        let controlBlurs: Observable<any>[] = this.formInputElements
            .map((formControl: ElementRef) => Observable.fromEvent(formControl.nativeElement, 'blur'));

        // Merge the blur event observable with the valueChanges observable
        Observable.merge(this.loginForm.valueChanges, ...controlBlurs).debounceTime(800).subscribe(value => {
            this.displayMessage = this.genericValidator.processMessages(this.loginForm);
        });
    }

    loginProcess() {
        this.loading = true;
        let c = Object.assign({}, this.login, this.loginForm.value);
        this.authService.loginProcess(this.loginForm.value)
            .subscribe(result => {
                if (result === true) {
                    this.router.navigate(['/']);
                } else {
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            });
    }
}
