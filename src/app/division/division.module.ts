﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { DivisionListComponent } from './division-list.component';
import { DivisionDetailComponent } from './division-detail.component';
import { DivisionEditComponent } from './division-edit.component';
import { DivisionFilterPipe } from './division-filter.pipe';

import { DivisionDetailGuard, DivisionEditGuard } from './division-guard.service';
import { DivisionService } from './division.service';
import { SharedModule } from '../shared/shared.module';

import { AuthGuard } from '../auth/auth.guard';

@NgModule({
    imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'divisions', component: DivisionListComponent, canActivate: [AuthGuard] },
            { path: 'division/:id', component: DivisionDetailComponent, canActivate: [DivisionDetailGuard, AuthGuard] },
            { path: 'divisionEdit/:id', component: DivisionEditComponent, canDeactivate: [DivisionEditGuard, AuthGuard] }
        ])
    ],
    entryComponents: [

    ],
    declarations: [
        DivisionListComponent,
        DivisionDetailComponent,
        DivisionEditComponent,
        DivisionFilterPipe
    ],
    providers: [
        DivisionService,
        DivisionDetailGuard,
        DivisionEditGuard,
        AuthGuard,
    ]
})
export class DivisionModule { }
