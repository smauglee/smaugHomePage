﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';

import { Const } from './../shared/const.component';
import { IDivision } from '../models/division';
import { ISelectList } from './../shared/selectList';

@Injectable()
export class DivisionService {
    private API_ENDPOINT = Const.BASE_ENDPOINT + 'MstDivision';
    headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON

    constructor(private http: Http) { }

    initializeDivision(): IDivision {
        // Return an initialized object
        return {
            division_id: null,
            division_name: null,
            create_date: null,
            create_user_id: null,
            update_date: null,
            update_user_id: null
        };
    }

    // list
    getDivisions(): Observable<IDivision[]> {
        console.log(this.API_ENDPOINT);
        return this.http.get(this.API_ENDPOINT)
            .map(this.extractData)
            .do(data => console.log('All: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // detail or edit
    getDivision(id: string): Observable<IDivision> {
        if (id == "0") {
            return Observable.create((observer: any) => {
                observer.next(this.initializeDivision());
                observer.complete();
            });
        }
        return this.http.get(`${this.API_ENDPOINT}/${id}`)
            .map(this.extractData)
            .do(data => console.log('division: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // delete
    deleteDivision(id: string): Observable<Response> {
        let options = new RequestOptions({ headers: this.headers });

        const url = `${this.API_ENDPOINT}/${id}`;
        return this.http.delete(url, options)
            .do(data => console.log('deleteDivision: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // post or put
    saveDivision(division: IDivision): Observable<IDivision> {
        let options = new RequestOptions({ headers: this.headers });

        if (division.create_date == null) {
            return this.createDivision(division, options);
        }
        return this.updateDivision(division, options);
    }

    // post
    private createDivision(division: IDivision, options: RequestOptions): Observable<IDivision> {
        //division.division_id = undefined;
        return this.http.post(this.API_ENDPOINT, division, options)
            .map(this.extractData)
            .do(data => console.log('createDivision: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // put
    private updateDivision(division: IDivision, options: RequestOptions): Observable<IDivision> {
        const url = `${this.API_ENDPOINT}/${division.division_id}`;
        //const url = `${this.API_ENDPOINT}`;
        return this.http.put(url, division, options)
            .map(() => division)
            .do(data => console.log('updateDivision: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // division_select_list
    getDivisionList(): Observable<ISelectList[]> {
        console.log(Const.BASE_ENDPOINT + 'MstDivision');
        return this.http.get(Const.BASE_ENDPOINT + 'MstDivision')
            .map(this.extractData)
            .do(data => console.log('All Division: ' + JSON.stringify(data)))
            .catch(this.handleError);
    }

    // dataset
    private extractData(response: Response) {
        let body = <IDivision[]>response.json();
        return body || {};
    }

    // error
    private handleError(error: Response): Observable<any> {
        // in a real world app, we may send the server to some remote logging infrastructure
        // instead of just logging it to the console
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}
