﻿export interface IDivision {
    division_id: string;
    division_name: string;
    create_date: string;
    create_user_id: string;
    update_date: string;
    update_user_id: string;
}