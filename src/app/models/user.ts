﻿/* Defines the product entity */
export interface IUser {
    user_id: string;
    user_name: string;
    email: string;
    password: string;
    role_id: number;
    role_name: string;
    division_id: number;
    division_name: string;
    tel_no: string;
    birthday: string;
    post_no: string;
    city_id: number;
    city_name: string;
    address_1: string;
    address_2: string;
    sort_no: number;
    bank_id: number;
    bank_name: string;
    bank_account_type: number;
    bank_account_no: string;
    bank_branch_no: number;
    bank_branch_name: string;
    create_date: string;
    update_date: string;
    create_user_id: string;
    update_user_id: string;
}

